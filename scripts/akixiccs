#!/bin/bash

readonly sAkixiHome=${HOME}/.akixi
readonly sAkixiCCSVersion="${sAkixiHome}/image_version"
readonly sRedHatPackageManager="yum"
readonly sAkixiScript="/usr/bin/akixiccs"
readonly sProgramName=$(basename ${sAkixiScript})
readonly sProgramVersion="v0.0.1"
readonly sAkixiAnsibleUrl="https://bitbucket.org/akixilimited/on-premise-deployments.git"
readonly sAkixiCCSSrc="https://bitbucket.org/akixilimited/on-premise-deployments/raw/master/scripts/akixiccs"
readonly sAkixiAnsibleHosts="${sAkixiHome}/akixi_local"
readonly sAkixiName="akixiccs"
readonly sAkixiImageName="akixiltd/akixiccs"
readonly sAkixiImageRegistry="docker.io"
readonly sAkixiContainerCmd="docker"
readonly sAkixiBashCompletionScript="/etc/bash_completion.d/akixi-completion.bash"
readonly sAkixiConfigDir="/etc/akixiccs"
sAkixiDataDir="/vol1"
sAkixiLastImageVer="latest"
export AKIXICCS_DOCKER_USERNAME="akixideployments"

#
# Prints help
#
function help() {
      echo "Usage:"

      echo "    ${sProgramName} start                 Start AkixiCCS"
      echo "    ${sProgramName} stop                  Stop AkixiCCS"
      echo "    ${sProgramName} restart               Restart AkixiCCS"
      echo "    ${sProgramName} reload                Reloads configuration for AkixiCCS dependent services including PostgreSQL, Tomcat, HTTPD, Postfix, NetData, Cron and Logrotate."
      echo "    ${sProgramName} init                  Init AkixiCCS"
      echo "    ${sProgramName} status [all]          Status of AkixiCCS. Option 'full' will show all services info"
      echo "    ${sProgramName} enter                 Enter to AkixiCCS environment"
      echo "    ${sProgramName} update <version>      Update to specific <version>. Default is 'latest'"
      echo "    ${sProgramName} exec <cmd>            Exec command in AkixiCCS env. Default is '/bin/bash'"
      echo "    ${sProgramName} clean [all]           Clean action. Option 'all' will also remove image"
      echo "    ${sProgramName} uninstall [all]       Uninstall akixi script. Option 'all' will also remove '${sAkixiHome}''"
      echo "    ${sProgramName} slave                 Init slave instance"
      echo "    ${sProgramName} db [backup|restore]   Database actions"
      echo "    ${sProgramName} lic [sign|verify]     Sign and verify license"
      echo "    ${sProgramName} -h                    Display this help message."
      echo "    ${sProgramName} -v                    Shows version of script."
      echo ""
      exit 0
}
#
# Prints version of the script
#
function PrintVersion() {
      echo "$(basename ${sAkixiScript}) ${sProgramVersion}"
      exit 0
}

#
# Check if system is redhat/centos version 7 based
#
function CheckSystem() {
  echo "Checking system ..."
  local sReleaseFile="/etc/redhat-release"
  [ -f ${sReleaseFile} ] || { echo "Only the RHEL7 or Centos 7 distributions are supported. Exiting."; exit 1; }
  local sOsName=$(cat ${sReleaseFile} | cut -d ' ' -f 1)
  local sOsVersion=$(cat ${sReleaseFile} | cut -d ' ' -f 4)
  [[ "${sOsName}" = "CentOS" || "${sOsName}" = "RedHat" ]] || { echo "Only the RHEL7 or Centos 7 distributions are supported. Exiting."; exit 2; }
  [[ "${sOsVersion:0:1}" = "7" ]] || { echo "Only the RHEL7 or Centos 7 distributions are supported. Exiting."; exit 3; }
  [[ "$(id -u)" = "0" ]] || { echo "Run this command as 'root'. Exiting."; exit 4; }
  systemctl stop postfix.service
}

function CheckDirectories() {
  [ -d ${sAkixiConfigDir} ] || { echo "Error: Config directory '${sAkixiConfigDir}' is missing. Please create configuration files before proceeding."; exit 1; }
  [ -n "${AKIXICCS_STORGE_VOLUME}" ] && sAkixiDataDir=${AKIXICCS_STORGE_VOLUME}
  [ -d ${sAkixiDataDir} ] || { echo "Error: Data directory '${sAkixiDataDir}' is missing. Please create it before proceeding. We recommend that this is placed on a seperate data partition."; exit 2; }
}

#
# Generate default configuration to run ansible instructions on local machine
#
function GenerateAnsibleLocalHostFile() {

  if [ ! -f ${sAkixiAnsibleHosts} ];then
  cat <<EOF >> ${sAkixiAnsibleHosts}
[local]
$(hostname -f) connection=local
EOF
  fi
}

#
# Handle 'akixiccs init' option
#   - Install ansible and  git if needed
#   - Download latest scripts from master branch of project at https://bitbucket.org/akixilimited/on-premise-deployments.git
#   - Execute ansible playbook to initialize host machine
#   - Source env variables for auto complete to work
#
function InitAkixiccs() {
  echo "Init ..."
  local sPackages=""
  rpm -q ansible > /dev/null 2>&1 || sPackages="${sPackages} ansible"
  rpm -q git > /dev/null 2>&1 || sPackages="${sPackages} git"
  [ -n "${sPackages}" ] && ${sRedHatPackageManager} install -y ${sPackages}

  UpdateAkixiccsScript
  [ -f ${sAkixiBashCompletionScript} ] && source ${sAkixiBashCompletionScript}
}

#
# Updates scripts and ansible stuff from master on project on-premise-deployments
#   - Download latest scripts from master branch of project at https://bitbucket.org/akixilimited/on-premise-deployments.git
#   - Execute ansible playbook to initialize host machine
#
function UpdateAkixiccsScript() {
  ansible-pull -U ${sAkixiAnsibleUrl} -i ${sAkixiAnsibleHosts}
}

#
# Prints status of akixiccs service
# If 'all' parameters is added it will also print status of all subservices
#
function StatusAkixiccs() {
  ${sAkixiContainerCmd} container ls -a --filter name=${sAkixiName}
  ExecCmdInAkixiccs systemctl status ${sAkixiName}.service
  if [[ "$1" = "all" ]];then
    ExecCmdInAkixiccs systemctl status postgresql-11.service httpd.service
  fi

}

#
# Starts of akixiccs service (docker container)
#
function StartAkixiccs() {
  echo "Starting ..."
  CheckDirectories
  CheckDockerHubLogin
  ${sAkixiContainerCmd} pull ${sAkixiImageRegistry}/${sAkixiImageName}:${sAkixiLastImageVer} || { echo "Problem pulling image '${sAkixiImageRegistry}/${sAkixiImageName}:${sAkixiLastImageVer}' !!! "; exit 1; }
  # check if created
  if [[ -z "$(${sAkixiContainerCmd} container ls -a --filter name=${sAkixiName} -q)" ]];then
    ${sAkixiContainerCmd} run -d  --privileged  --net=host  --restart=always -e container="docker" --name ${sAkixiName}  -v ${sAkixiDataDir}:/vol1  -v /etc/akixiccs:/etc/akixiccs  ${sAkixiImageRegistry}/${sAkixiImageName}:${sAkixiLastImageVer}
  else
    ${sAkixiContainerCmd} start ${sAkixiName}
  fi
}

#
# Stops of akixiccs service (docker container)
#
function StopAkixiccs() {
  echo "Stoping ..."
  ${sAkixiContainerCmd} stop ${sAkixiName}
}

#
# Restarts of akixiccs service (docker container)
#
function RestartAkixiccs() {
    StopAkixiccs
    StartAkixiccs
}

#
# Updates to latest docker image and  start akixiccs service
#   - Finds out version user wants to use. Default is 'latest'. Current version used is stored in $HOME/.akixi/info
#   - Clean and stop previous version
#   - Download coresponding image
#   - Start akixiccs service with coresponding version
#
function UpdateAkixiccs() {
    local ver=$1
    [ -z "$ver" ] && ver="${sAkixiLastImageVer}"
    [ -z "$ver" ] && ver="latest"
    echo "Updating to '$ver'..."
    echo "$ver" > ${sAkixiCCSVersion}
    sAkixiLastImageVer=$ver
    CheckDockerHubLogin
    CleanImagesAkixiccs
    ${sAkixiContainerCmd} pull ${sAkixiImageRegistry}/${sAkixiImageName}:${sAkixiLastImageVer} || { echo "Problem pulling image '${sAkixiImageRegistry}/${sAkixiImageName}:${sAkixiLastImageVer}' !!! "; exit 1; }
    CleanAkixiccs
    StartAkixiccs
}

#
# Cleans docker container
#   - If 'all' option is added it will also remove image
#
function CleanAkixiccs() {
    echo "Cleaning opt='$1' ..."
    [[ -n "$(${sAkixiContainerCmd} container ls -a --filter name=${sAkixiName} -q)" ]] && ${sAkixiContainerCmd} rm -f akixiccs
    CleanImagesAkixiccs $1
}

#
# Cleans docker container
#   - If 'all' option is added it will also remove image
#
function CleanImagesAkixiccs() {
    echo "Cleaning images opt='$1' ..."
    sCurrentImageID=$(${sAkixiContainerCmd} images --format "{{.ID}}" --filter=reference=${sAkixiImageName}:${sAkixiLastImageVer})
    for d in $(${sAkixiContainerCmd} images --format "{{.ID}}_{{.Repository}}:{{.Tag}}" --filter=reference=${sAkixiImageName} | grep -v ${sAkixiImageName}:${sAkixiLastImageVer}); do
      img=$(echo "$d" | cut -d '_' -f1)
      if [ "${sCurrentImageID}" != "$img" ];then
        echo "Removing unused image '$(echo "$d" | cut -d '_' -f2)' ..."
        ${sAkixiContainerCmd} rmi -f $img > /dev/null 2>&1
      fi
    done
    [[ "$1" = "all" ]] && ${sAkixiContainerCmd}  rmi ${sAkixiImageRegistry}/${sAkixiImageName}:${sAkixiLastImageVer}
}

#
# Reload `akixiccs` service
#
function ReloadAkixiccs() {
    ExecCmdInAkixiccs systemctl reload ${sAkixiName}.service
}

#
# Reinstall `akixiccs` script
#
function ReinstallAkixiccs() {
    UninstallAkixiccs
    InitAkixiccs
}

#
# Prepare master machine for slave machine
#
function MasterAkixiccs() {
    echo "Applying setting for master [$1]"
    if [ -z "${AKIXICCS_DB_SLAVE_IP}" ];then
      echo "Please set slave ip first by adding next line to ${sAkixiConfigDir}/config before continuing."
      echo "    export AKIXICCS_DB_SLAVE_IP=<ip>"
      exit 1
    fi
    ReloadAkixiccs
}

#
# Start process on slave 
#   - Check port 5432/tcp is open on master machine
#   - Start db sync process
#   - Start AkixiCCS service with as now copy of master. Slave is separate instance and master can be removed.
#
function SlaveAkixiccs() {
    echo "Sync slave [$1]"
    if [ -z "${AKIXICCS_DB_MASTER_IP}" ];then
      echo "Please set master ip first by adding next line to ${sAkixiConfigDir}/config before continuing."
      echo "    export AKIXICCS_DB_MASTER_IP=<ip>"
      exit 1
    fi
    StopAkixiccs
    sPgPortOpen=$(nmap -p 5432 ${AKIXICCS_DB_MASTER_IP} -Pn | grep 5432/tcp | cut -d ' ' -f2)
    [ "${sPgPortOpen}" == "open" ] || { echo "Port '5432' is not open (nmap_status='${sPgPortOpen}') on '${AKIXICCS_DB_MASTER_IP}'. Open it first or start 'postgresql-11.service' service on master machine ('${AKIXICCS_DB_MASTER_IP}')."; exit 2; }
    ExecCmdInAkixiccsOneTime /opt/AkixiCCS/bin/AkixiCCSDbSync.sh
    StartAkixiccs
}
#
# DB data handling
#
function DbAkixiccs() {
    # echo "Db action [$*]"
    case "$1" in
      backup)
        ExecCmdInAkixiccs /opt/AkixiCCS/bin/AkixiCCSDbBackup.sh
        ;;
      restore)
        if [ "$2" = "help" ];then
          ExecCmdInAkixiccs /opt/AkixiCCS/bin/AkixiCCSDbRestore.sh help
          exit 0
        else
          shift
          ExecCmdInAkixiccs /opt/AkixiCCS/bin/AkixiCCSDbRestore.sh $*
        fi
        ;;
      *)
        echo "AkixiCCs db (database) action"
        echo ""
        echo "Options:"
        echo ""
        echo "  backup"
        echo "  restore <restore-arguments>"
        echo "  restore help"
        echo ""
        ;;
    esac

}

#
# Sys backup
#
function AdminAkixiccs() {
    # echo "Db action [$*]"
    case "$1" in
      backup)
        ExecCmdInAkixiccs /opt/AkixiCCS/bin/AkixiCCSAdminBackup.sh
      ;;
      clean)
        iNBackup=0
        [ -z "${AKIXICCS_ADMIN_BACKUP_N}" ] || iNBackup=${AKIXICCS_ADMIN_BACKUP_N}
        [ "${iNBackup}" == 0 ] && { echo "No backup files were removed !!!"; exit 0; }
        echo "Cleaning admin backup files ..."
        echo "${iNBackup} backup file will remain on system"
        ls -tp ${sAkixiDataDir}/akixi/Backup/akixiccs-admin-backup-*.tar.gz> /dev/null 2>&1
        if [ $? -eq 0 ];then
          iN=$(ls -tp ${sAkixiDataDir}/akixi/Backup/akixiccs-admin-backup-*.tar.gz | wc -l)
          [ ${iN} -gt ${iNBackup} ] && ls -tp ${sAkixiDataDir}/akixi/Backup/akixiccs-admin-backup-*.tar.gz | awk 'NR>'${iNBackup}'' | xargs rm
        fi
        iN=$(ls -tp ${sAkixiDataDir}/akixi/Backup/akixiccs-admin-backup-*.tar.gz | wc -l)
        [ ${iN} -gt 0 ] && ls -al ${sAkixiDataDir}/akixi/Backup/akixiccs-admin-backup-*.tar.gz || echo "No backup found !!!"
        echo "done"
      ;;
      *)
        echo "AkixiCCs admin action"
        echo ""
        echo "Options:"
        echo ""
        echo "  backup"
        echo "  clean"
        echo ""
        ;;
    esac

}

#
# Remove akixiccs scripts
#
function UninstallAkixiccs() {
    rm -f ${sAkixiScript}
    rm -f ${sAkixiBashCompletionScript}
    [[ "$1" = "all" ]] && rm -rf ${sAkixiHome}
    echo "Uninstall done"
    echo "To reinstall run:"
    echo ""
    echo "   bash <(curl -s ${sAkixiCCSSrc})"
    echo ""
    exit 0
}

#
# Execute command in akixiccs container
#
function ExecCmdInAkixiccs() {
    [ -z "$1" ] && { EnterToAkixiccs; exit 0; }
    ${sAkixiContainerCmd} exec -it ${sAkixiName} /bin/bash -c "$*"
}

#
# Execute command in akixiccs image
#
function ExecCmdInAkixiccsOneTime() {
    [ -z "$1" ] && { echo "Error : command is missing."; exit 1; }
    ${sAkixiContainerCmd} run -it --rm -v /etc/akixiccs:/etc/akixiccs -v ${sAkixiDataDir}:/vol1 docker.io/akixiltd/akixiccs:latest /bin/bash -c "$*"
}


#
# Enter to akixiccs container
#
function EnterToAkixiccs() {
  ${sAkixiContainerCmd} exec -it ${sAkixiName} /bin/bash
}

#
# Check for docker hub info like username and token
#
function CheckDockerHubInfo() {
    [ -z "${AKIXICCS_DOCKER_USERNAME}" ] && { echo "DokerHub username is missing !!! Please set '\$AKIXICCS_DOCKER_USERNAME' first. "; exit 1; }
    [ -z "${AKIXICCS_DOCKER_TOKEN}" ] && { echo "DokerHub token for user '${AKIXICCS_DOCKER_USERNAME}' is missing !!! Please set '\$AKIXICCS_DOCKER_TOKEN' first. "; exit 1; }
}


#
# Check if user is loged in docker.io
#
function CheckDockerHubLogin() {
    if [ -f ~/.docker/config.json ];then
      if ! grep -q "docker.io" ~/.docker/config.json ; then
          LoginToDockerHub
      fi
    else
      LoginToDockerHub
    fi
}

#
# This function will login user to docker hub (docker.io). Following variables needs to be set first. One can export it or put it to /etc/akixiccs/config file
#   - username : \$AKIXICCS_DOCKER_USERNAME
#   - token: \$AKIXICCS_DOCKER_TOKEN
#
function LoginToDockerHub() {
    CheckDockerHubInfo
    echo "${AKIXICCS_DOCKER_TOKEN}" | ${sAkixiContainerCmd} login -u ${AKIXICCS_DOCKER_USERNAME} --password-stdin docker.io || exit 1
}

#
# This function will logout from docker hub (docker.io). Following variables needs to be set first. One can export it or put it to /etc/akixiccs/config file
#   - username : \$AKIXICCS_DOCKER_USERNAME
#   - token: \$AKIXICCS_DOCKER_TOKEN
#
function LogoutToDockerHub() {
    CheckDockerHubInfo
    ${sAkixiContainerCmd} logout || exit 1
}

function LixAkixiccsHelp() {
        echo "AkixiCCs lic action"
        echo ""
        echo "Options:"
        echo ""
        echo "  sign /etc/akixiccs/<input-license> /etc/akixiccs/<path-to-key.pem>"
        echo "  verify /etc/akixiccs/<input-license> # Default productioncertificate will be used from /opt/AkixiCCS/license/cert.pem"
        echo "  verify /etc/akixiccs/<input-license> <path-to-cert.pem>"
        echo ""
        echo "Note : Use '/etc/akixiccs' directory as input directory for all files and specify them with respect to this directory."
        echo ""
        exit 1
}
function LixAkixiccs() {
    case "$1" in
      sign)
        shift
        [[ $# -eq 2 ]] || { echo "Error: Not enough arguments !!!"; echo ""; LixAkixiccsHelp; }
        [ -f ${sAkixiConfigDir}/$1 ] || { echo "Error: File '${sAkixiConfigDir}/$1' was not found "; exit 1; }
        [ -f ${sAkixiConfigDir}/$2 ] || { echo "Error: File '${sAkixiConfigDir}/$2' was not found "; exit 1; }
        ExecCmdInAkixiccs node /opt/AkixiCCS/license/akixi-license.js ${sAkixiConfigDir}/$1 ${sAkixiConfigDir}/$2 ${sAkixiConfigDir}/AppLicense-new.xml
        ;;
      verify)
        shift
        [[ $# -lt 1 ]] && { echo "Error: Not enough arguments !!!"; echo ""; LixAkixiccsHelp; }
        [ -f ${sAkixiConfigDir}/$1 ] || { echo "Error: File '${sAkixiConfigDir}/$1' was not found "; exit 1; }
        sCertFile="${sAkixiConfigDir}/$2"
        [[ $# -eq 1 ]] &&  sCertFile="/opt/AkixiCCS/license/cert.pem"
        ExecCmdInAkixiccs node /opt/AkixiCCS/license/akixi-license-verify.js ${sAkixiConfigDir}/$1 ${sCertFile}
        ;;
      *)
        LixAkixiccsHelp
        ;;
    esac
}

CheckSystem

[ -d ${HOME}/.akixi ] || mkdir -p ${HOME}/.akixi/

GenerateAnsibleLocalHostFile
[ -f ${sAkixiScript} ] || { InitAkixiccs; exit 0; }
[ -f ${sAkixiCCSVersion} ] && sAkixiLastImageVer=$(cat ${sAkixiCCSVersion})

[ -f ${sAkixiConfigDir}/config ] && source ${sAkixiConfigDir}/config
[ -z "$1" ] && help

while getopts ":hv" opt; do
  case ${opt} in
    h )
      help
      ;;
    v )
      PrintVersion
      ;;
    \? )
      echo "Invalid Option: -$OPTARG" 1>&2
      exit 1
      ;;
  esac
done
shift $((OPTIND -1))


subcommand=$1; shift
case "$subcommand" in
  init)
    InitAkixiccs
    ;;
  status)
    StatusAkixiccs $1
    ;;
  start)
    StartAkixiccs
    ;;
  stop)
    StopAkixiccs
    ;;
  restart)
    RestartAkixiccs
    ;;
  reload)
    ReloadAkixiccs
    ;;
  clean)
    CleanAkixiccs $1
    ;;
  login)
    LoginToDockerHub
    ;;
  logout)
    LogoutToDockerHub
    ;;
  update)
    UpdateAkixiccs $1
    ;;
  enter)
    EnterToAkixiccs
    ;;
  exec)
    ExecCmdInAkixiccs $*
    ;;
  uninstall)
    UninstallAkixiccs $1
    ;;
  master)
    MasterAkixiccs $1
    ;;
  slave)
    SlaveAkixiccs $1
    ;;
  db)
    DbAkixiccs $*
    ;;
  admin)
    AdminAkixiccs $*
    ;;
  lic)
    LixAkixiccs $*
    ;;
esac
