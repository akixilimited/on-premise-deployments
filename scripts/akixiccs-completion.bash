#/usr/bin/env bash

_akixiccs() {
  COMPREPLY=()

  # All possible first values in command line
  local COMMANDS=("init" "start" "stop" "restart" "reload" "status" "update" "enter" "exec" "clean" "uninstall" "master" "slave" "db" "admin" "-v" "-h")

  # declare an associative array for options
  declare -A ACTIONS
  ACTIONS[status]="all"
  ACTIONS[update]="latest 2.1.0.1"
  ACTIONS[clean]="all"
  ACTIONS[start]=""
  ACTIONS[stop]=""
  ACTIONS[db]="backup restore list"
  ACTIONS[restart]=""
  ACTIONS[reload]=""
  ACTIONS[master]=""
  ACTIONS[slave]=""
  ACTIONS[admin]="backup clean"

  # All possible options at the end of the line
  local OPTIONS=()

  # current word being autocompleted
  local cur=${COMP_WORDS[COMP_CWORD]}

  # If previous arg is -v it means that we remove -v from COMMANDS for autocompletion
  if [ $3 = "-v" ] ; then
    COMMANDS=${COMMANDS[@]:1}
  fi

  # If previous arg is a key of ACTIONS (so it is a service).
  # It means that we must display action choices
  if [ ${ACTIONS[$3]+1} ] ; then
    COMPREPLY=( `compgen -W "${ACTIONS[$3]}" -- $cur` )
  # If previous arg is one of the actions or previous arg is an option
  # We are at the end of the command and only options are available
  elif [[ "${ACTIONS[*]}" == *"$3"* ]] || [[ "${OPTIONS[*]}" == *"$3"*  ]]; then
    # SPecial use case : help does not support options
    if [ "$3" != "help" ] ; then
      COMPREPLY=( `compgen -W "${OPTIONS[*]}" -- $cur` )
    fi
  else
    # if everything else does not match, we are either :
    # - first arg waiting for -v or a service code
    # - second arg with first being -v. waiting for a service code.
    COMPREPLY=( `compgen -W "${COMMANDS[*]}" -- $cur` )
  fi
}

complete -F _akixiccs akixiccs
